* my first Node cli

* using the [compress-json](https://github.com/beenotung/compress-json/) package

* install it with (might need admin rights):
```
npm install -g gitlab:urswilke/compress_json_cli
```

Then you can use it with

```
compress_json <path/to/your/input/json/file.json> [<path/to/your/output/json/file.json>]
```

The result will be the compressed version of the input json file. If no output is specified, the result will be:

```
<path/to/your/input/json/file_out.json> 
```
