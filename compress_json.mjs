#!/usr/bin/env node
import { readFileSync, writeFileSync } from 'fs'
import { compress } from 'compress-json'
import { resolve } from 'path'
const args = process.argv;
if (args.length < 3) {
    console.error('Expected at least one argument!');
    process.exit(1);
}

let input_file = args[2];
console.log("Input file: " + input_file);
const file_ext = input_file.split('.').pop();
if (file_ext !== "json") {
    console.error('Error: Please pass a json file!');
    process.exit(1);
}
input_file = resolve(input_file);
console.log("Absolute path: " + input_file);
var obj = JSON.parse(readFileSync(input_file, 'utf8'));

var output_file;
if (args.length === 3) {
    output_file = input_file.replace(/\.[^/.]+$/, "_out.json")
} else {
    output_file = args[3]
    const outfile_ext = output_file.split('.').pop();
    if (outfile_ext !== "json") {
        console.error('Error: Please pass a valid json file for the output file!');
        process.exit(1);
    }
}
output_file = resolve(output_file);
console.log("Output file: " + output_file);

console.log("Compressing input file...");
var compressed_json = compress(obj) 
var json = JSON.stringify(compressed_json);
console.log("Writing output file...");
writeFileSync(output_file, json);
